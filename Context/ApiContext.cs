using Microsoft.EntityFrameworkCore;
using api_tasks.Models;

namespace api_tasks.Context
{
    public class ApiContext: DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options) : base(options)
        {
            
        }

        public DbSet<Goal> Goals { get; set; }
    }
}