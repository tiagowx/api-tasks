import { Box, Button, TextField } from "@mui/material";
import IGoal from "interfaces/IGoal";
import GoalsTable from "pages/components/GoalsTable";
import React, { useEffect, useState } from "react";
import ServiceTasks from "services/ServiceTasks";

const Home: React.FC = () => {
  const api = new ServiceTasks();
  const [goals, setGoals] = useState<IGoal[] | undefined>(undefined);
  const [search, setSearch] = useState<string>("");

  // eslint-disable-next-line
  async function handlerGetAll() {
    setGoals(await api.getGoalsAll());
    console.log(goals);
  }

  async function handlerSearch(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    console.log(search);
    setGoals(await api.getGoalsByTitle(search));
    console.log(goals);

  }

  useEffect(() => {
    setTimeout(()=>{
      if(goals === undefined)
        handlerGetAll();
    },3000)
  });


  return (
    <Box
      component="section"
      sx={{
        justifySelf: 'flex-start',
      }}>
        <Box 
          component="form" onSubmit={handlerSearch}>
            <TextField value={search} onChange={e => setSearch(e.currentTarget.value)}></TextField>
            <Button type="submit">
              Buscar
            </Button>
        </Box>
      <GoalsTable goals={goals} />
    </Box>
  );
}

export default Home;