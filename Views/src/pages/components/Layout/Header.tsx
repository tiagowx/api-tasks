import { AppBar, Toolbar, Typography } from "@mui/material";
import app from "configs/app";

const Header = () => {
  return (
    <AppBar
      component="header"
    >
      <Toolbar>
        <Typography variant="h2" component="h1" color="inherit" noWrap>
          {app.AppName}
        </Typography>
      </Toolbar>

    </AppBar>
  );
}

export default Header;