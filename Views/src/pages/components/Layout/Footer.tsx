import { Box, Typography } from "@mui/material";
import app from "configs/app";

const Footer: React.FC = () => {
  return (
    <Box
      component="footer"
      sx={{
        textAlign: "center",
      }}>
      <Typography component="span">
        &copy; {app.AppName} | {new Date().getFullYear()}
      </Typography>
    </Box>
  );
}

export default Footer;
