import { Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material";
import IGoal from "interfaces/IGoal";

interface Props {
  goals: IGoal[]|undefined;
}

const GoalsTable: React.FC<Props> = (props: Props) => {

  return (
    <Table>
      <TableHead>
        <TableRow >
          <TableCell sx={{fontWeight:"bold"}}>ID</TableCell>
          <TableCell sx={{fontWeight:"bold"}}>Título</TableCell>
          <TableCell sx={{fontWeight:"bold"}}>Descrição</TableCell>
          <TableCell sx={{fontWeight:"bold"}}>Data</TableCell>
          <TableCell sx={{fontWeight:"bold"}}>Status</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {props.goals && props.goals.map(item => 
          <TableRow key={item.id}>
            <TableCell>{item.id}</TableCell>
            <TableCell>{item.title}</TableCell>
            <TableCell>{item.description}</TableCell>
            <TableCell>{new Date(item.date.toString()).toLocaleDateString("bt-br")}</TableCell>
            <TableCell>{item.status}</TableCell>
          </TableRow>
        )}
      </TableBody>

    </Table>
  );
};

export default GoalsTable;