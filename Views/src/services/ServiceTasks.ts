import axios from 'axios';
import GoalStatus from 'enums/GoalStatus';
import app from '../configs/app';

const instance = axios.create(app.configs);

class ServiceTasks {
  getGoalsAll = async () => {
    const result = await instance.get(`/Goal/ObterTodos`)
      .then((res) => res);
    const { data } = result;
    return data;
  }
  getGoalsByTitle = async (title: string) => {
    const result = await instance.get(`/Goal/ObterPorTitulo`, {params:{title}})
      .then((res) => res);
    const { data } = result;
    console.log(data);
    return data;
  }
  getGoalsByDescription = async (description: string) => {
    const result = await instance.get(`/Goal/ObterPorDescricao`, {params:{description}})
      .then((res) => res);
    const { data } = result;
    console.log(data);
    return data;
  }
  getGoalsByDate = async (date: Date) => {
    const result = await instance.get(`/Goal/ObterPorTitulo`, {params:{date}})
      .then((res) => res);
    const { data } = result;
    console.log(data);
    return data;
  }
  getGoalsByStatus = async (status: GoalStatus) => {
    const result = await instance.get(`/Goal/ObterPorTitulo`, {params:{status}})
      .then((res) => res);
    const { data } = result;
    console.log(data);
    return data;
  }
}

export default ServiceTasks;