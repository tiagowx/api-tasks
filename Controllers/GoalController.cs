using Microsoft.AspNetCore.Mvc;
using api_tasks.Context;
using api_tasks.Models;

namespace api_tasks.Controllers
{    
    [ApiController]
    [Route("[controller]")]
    public class GoalController : ControllerBase
    {
        private readonly ApiContext _context;

        public GoalController(ApiContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            // TODO: Buscar o Id no bank utilizando o EF
            var goal = _context.Goals.Find(id);

            // TODO: Validar o tipo de retorno. Se não encontrar a goal, retornar NotFound,
            if(goal == null){
                return NotFound();
            }

            // caso contrário retornar OK com a goal encontrada
            return Ok(goal);
        }

        [HttpGet("ObterTodos")]
        public IActionResult GetAll()
        {
            // TODO: Buscar todas as goals no bank utilizando o EF
            var goals = _context.Goals.ToList();

            return Ok(goals);
        }

        [HttpGet("ObterPorTitulo")]
        public IActionResult GetByTitle(string title)
        {
            if(title == null){
                return NoContent();
            }

            // TODO: Buscar  as goals no bank utilizando o EF, que contenha o titulo recebido por parâmetro
            // Dica: Usar como exemplo o endpoint ObterPorData
            var goals = _context.Goals.Where(x => x.Title.Contains(title)).ToList();

            return Ok(goals);
        }

        [HttpGet("ObterPorDescricao")]
        public IActionResult GetByDescription(string description)
        {
            // TODO: Buscar  as goals no bank utilizando o EF, que contenha o titulo recebido por parâmetro
            // Dica: Usar como exemplo o endpoint ObterPorData
            var goals = _context.Goals.Where(x => x.Description.Contains(description)).ToList();

            return Ok(goals);
        }

        [HttpGet("ObterPorData")]
        public IActionResult GetByDate(DateTime date)
        {
            var goals = _context.Goals.Where(x => x.Date == date.Date).ToList();
            return Ok(goals);
        }

        [HttpGet("ObterPorStatus")]
        public IActionResult GetByStatus(EnumStatusGoal status)
        {
            // TODO: Buscar  as goals no bank utilizando o EF, que contenha o status recebido por parâmetro
            var goals = _context.Goals.Where(x => x.Status == status).ToList();

            // Dica: Usar como exemplo o endpoint ObterPorData
            return Ok(goals);
        }

        [HttpPost]
        public IActionResult Create(Goal goal)
        {
            if (goal.Date == DateTime.MinValue)
                return BadRequest(new { Erro = "A data da tarefa não pode ser vazia" });

            // TODO: Adicionar a goal recebida no EF e salvar as mudanças (save changes)
            _context.Goals.Add(goal);
            _context.SaveChanges();

            return CreatedAtAction(nameof(ObterPorId), new { id = goal.Id }, goal);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, Goal goal)
        {
            var goalBank = _context.Goals.Find(id);

            if (goalBank == null)
                return NotFound();

            if (goal.Date == DateTime.MinValue)
                return BadRequest(new { Erro = "A data da goal não pode ser vazia" });

            // TODO: Update as informações da variável goalBank com a goal recebida via parâmetro
            goalBank.Title = goal.Title;
            goalBank.Description = goal.Description;
            goalBank.Date = goal.Date;
            goalBank.Status = goal.Status;

            // TODO: Update a variável goalBank no EF e salvar as mudanças (save changes)
            _context.Goals.Update(goalBank);
            _context.SaveChanges();

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var goalBank = _context.Goals.Find(id);

            if (goalBank == null)
                return NotFound();

            // TODO: Remover a goal encontrada através do EF e salvar as mudanças (save changes)
            
            _context.Goals.Remove(goalBank);
            _context.SaveChanges();
            return NoContent();
        }
    }
    
}